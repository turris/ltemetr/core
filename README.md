> Ltemetr - LTE speed measurement application. Consists of network speed measurement tool and a simple HTTP server for controlling it.

# API

## `GET /status`

-> `HTTP 200 application/json`

```
{ "status": "running" }
{ "status": "stopped" }
```

## `GET /latest`
-> `HTTP 200 application/json`

```
{
    "time": "2019-04-13 12:58:54",
    "lat": 49.7491444444,
    "lon": 13.3797694444,
    "rsrp": -61,
    …
}
```

## `GET /export`
-> `HTTP 200 text/csv`

```
Time,Lat,Long,RSRP,SINR Rx[0],RSRQ,RSSI,IP Thrpt DL,MCC,MNC,LAC,Cell Id,PCI,DL EARFCN,Bandwidth,QPSK Rate,16-QAM Rate,64-QAM Rate,256-QAM Rate,Carrier Aggregation DL
03.04.2019 12:58:54,49.7491444444,13.3797694444,-61,26.2,-10.2,-31,8898,230,02,1419,204866817,219,0,10,,,,
```

## `POST /start`

-> `HTTP 200 application/json`

```
{
    "success": true,
    "status": "running"
}
```

```
{
    "success": false,
    "status": "running",
    "error": "already running"
}
```

```
{
    "success": false,
    "status": "stopped",
    "error": "…"
}
```

## `POST /stop`

-> `HTTP 200 application/json`

```
{
    "success": true,
    "status": "stopped"
}
```

```
{
    "success": false,
    "status": "stopped",
    "error": "already stopped"
}
```

```
{
    "success": false,
    "status": "running",
    "error": "…"
}
```

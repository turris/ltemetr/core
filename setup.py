#!/usr/bin/env python

#
# ltemetr-core
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from setuptools import setup

DESCRIPTION = "LTE benchmark and measurement application"

setup(
    name="ltemetr",
    version="0.3.1",
    author="CZ.NIC, z.s.p.o. (http://www.nic.cz/)",
    author_email="packaging@nic.cz",
    packages=[
        "ltemetr",
    ],
    url="https://gitlab.labs.nic.cz/turris/netmetr-client",
    license="GPLv3+",
    description=DESCRIPTION,
    long_description=open("README.md", encoding="utf-8").read(),
    install_requires=[
        "pexpect",
        "sentry_sdk",
        "requests",
        "netmetr",
    ],
    entry_points={
        "console_scripts": [
            "ltemetr = ltemetr.__main__:main",
        ]
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
)

import subprocess

from .config import config
from .exceptions import RunError, LtemetrError


def run_netmetr():
    try:
        retcode = subprocess.call(["netmetr", "--set-gps-console",
                                  config.get("modem_console_path")])
        if retcode != 0:
            raise RunError("Netmetr returned with error code={}".format(retcode))
    except (LtemetrError, FileNotFoundError) as e:
        raise RunError("Netmetr didn't run ({})".format(e))

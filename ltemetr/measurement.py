import datetime
import time

from .config import config
from .exceptions import RunError, LteError
from .iperf import Iperf
from .logging import get_logger
from .lteinfo import measure_lte
from .misc import distance, now
from .server import get_port

TIME_DIST = 1
TIME_DIST_FLOOD = 0.7  # Gap in case of iperf logs flood. 0.9s was too big

CSV_HEADER = "Time,Lat,Long,RSRP,SINR Rx[0],RSRQ,RSSI,TCP Thrpt DL,MCC,MNC,LAC,"\
             "Cell Id,PCI,DL EARFCN,Bandwidth,QPSK Rate,16-QAM Rate,64-QAM Rate,"\
             "256-QAM Rate,Carrier Aggregation DL"

logger = get_logger()


class MeasurementTypes:
    TRAIN = "TRAIN"
    CAR = "CAR"


def log(time, lte, dwl):
    """ Format output to an expected CSV like this:
    'Time,Lat,Long,RSRP,SINR Rx[0],RSRQ,RSSI,TCP Thrpt DL,MCC,MNC,LAC,\
    Cell Id,PCI,DL EARFCN,Bandwidth,QPSK Rate,16-QAM Rate,64-QAM Rate,\
    256-QAM Rate,Carrier Aggregation DL'
    """
    pattern = ("{time:%d.%m.%Y %H:%M:%S},{lat},{lon},{rsrp},{sinr},{rsrq},"
               "{rssi},{dwl:.2f},{mcc},{mnc},{lac},{cid},{pci},{dle},{bw}")

    lat = lte.get("lat", "")
    lon = lte.get("lon", "")

    if lat:
        lat = "{:.4f}".format(lat)

    if lon:
        lon = "{:.4f}".format(lon)

    return pattern.format(
        time=time,
        lat=lat,
        lon=lon,
        rsrp=lte.get("RSRP", ""),
        sinr=lte.get("SINR Rx[0]", ""),
        rsrq=lte.get("RSRQ", ""),
        rssi=lte.get("RSSI", ""),
        dwl=dwl,
        mcc=lte.get("MCC", ""),
        mnc=lte.get("MNC", ""),
        lac=lte.get("LAC", ""),
        cid=lte.get("Cell Id", ""),
        pci=lte.get("PCI", ""),
        dle=lte.get("DL EARFCN", ""),
        bw=lte.get("Bandwidth", "")
    )


class Measurement:
    def __init__(self, type=MeasurementTypes.TRAIN, stop_event=lambda: True,
                 duration=0):
        self.stop_event = stop_event
        self.log = [CSV_HEADER]
        self.last_kbps = 0
        self.last_lte = {}
        self.last_time = None
        self.type = type
        self._duration = duration
        self._last_logged_position = (0, 0)
        self.paused = False

    def _do_pause(self, current_position):
        """ The distance between current point and the last one is too short
        """
        if self.type == MeasurementTypes.CAR:
            return False

        dist = distance(self._last_logged_position, current_position)
        logger.debug("Distance to last log: {}m".format(dist))
        return dist <= config.get("max_distance_to_pause")

    def _update_stats(self, kbps, lte_data):
        _now = now()

        current_position = (lte_data.get("lat", 0), lte_data.get("lon", 0))
        self.paused = self._do_pause(current_position)

        if not self.paused:
            self.log.append(log(_now, lte_data, kbps))
            logger.debug("logging " + log(_now, lte_data, kbps))
            self._last_logged_position = current_position

        else:
            logger.debug("paused " + log(_now, lte_data, kbps))

        self.last_kbps = kbps
        self.last_lte = lte_data
        self.last_time = _now

    def _measure_lte(self):
        try:
            return measure_lte()

        except LteError as e:
            logger.exception("LteError (%s)", e)
            return {}

    def _duration_expired(self):
        if self._duration == 0:
            return False
        return (self.last_time - self.start_time >
                datetime.timedelta(seconds=self._duration))

    def _stop_condition(self):
        return self.stop_event() or self._duration_expired()

    def _get_delay(self):
        td = now() - self.last_time
        return td.total_seconds()

    def _is_too_early(self, delay):
        return delay > 0 and delay < TIME_DIST

    def _is_iperf_flood(self, delay):
        return delay > 0 and delay < TIME_DIST_FLOOD

    def measure(self):
        self.start_time = self.last_time = now()

        while not self._stop_condition():
            try:
                port = get_port()
                iperf = Iperf(port)

                for kbps in iperf.get_data():
                    delay = self._get_delay()
                    if not self._is_iperf_flood(delay):
                        lte_data = self._measure_lte()
                        self._update_stats(kbps, lte_data)

                    if self._stop_condition():
                        iperf.stop()
                        break

            except RunError as e:
                logger.error("Measurement Run Error - iperf not running "
                             "({})".format(e))

                delay = self._get_delay()
                if self._is_too_early(delay):
                    time.sleep(TIME_DIST - delay)

                lte_data = self._measure_lte()
                self._update_stats(0, lte_data)

        self.paused = True

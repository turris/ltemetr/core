import serial

from .config import config
from .exceptions import RunError, LteError
from .logging import get_logger

logger = get_logger()


def get_operator(modem_console_path):
    """ Example output
    AT+COPS?
    +COPS: 0,0,"T-Mobile CZ T-Mobile CZ",7
    """
    try:
        with serial.Serial(modem_console_path) as cons:
            cons.write(b"AT+COPS?\r")
            cons.readline()
            line = cons.readline().decode("utf-8")
            parts = line.split(",")
            if len(parts) >= 3:
                return "".join(parts[2].split()[0].split('"'))
            else:
                raise RunError("Wrong console output ({})".format(line))
    except serial.serialutil.SerialException as e:
        raise RunError(str(e)) from e


def get_gstatus(console):
    console.write(b"AT!GSTATUS?\r")
    value_map = {
        "LTE bw": "Bandwidth",
        "SINR (dB)": "SINR Rx[0]",
    }
    gstatus = {}
    while True:
        line = console.readline().decode("utf-8")
        if len(line) == 4:  # this is "OK\r\n"
            break
        for value_pair in line.split("\t"):
            value_pair = value_pair.split(":")
            value_name = value_map.get(value_pair[0], None)
            if value_name:
                gstatus[value_name] = value_pair[1].split()[0]

    return gstatus


def get_lteinfo(console):
    """ Example output:
    !LTEINFO:
    Serving:   EARFCN MCC MNC   TAC      CID Bd D U SNR PCI  RSRQ   RSRP   RSSI RXLV
                 6300 230  02  1419 0C360501 20 3 3  24 219  -8.9  -63.4  -35.4 --

    IntraFreq:                                          PCI  RSRQ   RSRP   RSSI RXLV
                                                        219  -8.9  -63.4  -35.4 --
                                                        215 -20.0  -83.5  -49.6 --
                                                          2 -20.0  -83.4  -50.4 --

    InterFreq: EARFCN ThresholdLow ThresholdHi Priority PCI  RSRQ   RSRP   RSSI RXLV

    WCDMA:     UARFCN ThreshL ThreshH Prio PSC   RSCP  ECN0 RXLV


    OK

    We are interested in the "Serving" line
    """
    lteinfo = {}
    console.write(b"AT!LTEINFO?\r")
    while True:
        line = console.readline().decode("utf-8")
        if len(line) < 3:
            continue
        split_line = line.split()
        if len(line) == 4:  # this is "OK\r\n"
            break
        if split_line[0] == "Serving:":
            line = console.readline().decode("utf-8")
            split_line = line.split()

            lteinfo["DL EARFCN"] = split_line[0]
            lteinfo["MCC"] = split_line[1]
            lteinfo["MNC"] = split_line[2]
            lteinfo["LAC"] = split_line[3]
            lteinfo["Cell Id"] = int(split_line[4], 16)
            lteinfo["PCI"] = split_line[9]
            lteinfo["RSRQ"] = split_line[10]
            lteinfo["RSRP"] = split_line[11]
            lteinfo["RSSI"] = split_line[12]

    return lteinfo


def get_gpsloc(console):
    console.write(b"AT!GPSLOC?\r")
    console.readline()
    while True:
        line = console.readline().decode("utf-8")
        if len(line) == 4:  # this is "OK\r\n"
            return {}
        line_split = line.split(":")
        if line_split[0] == "Lat":
            line_split = line_split[1].split()
            lat = (float(line_split[0]) +
                   float(line_split[2]) / 60 +
                   float(line_split[4]) / 3600)
            if line_split[6] == "S":
                lat = -lat
            continue
        if line_split[0] == "Lon":
            line_split = line_split[1].split()
            lon = (float(line_split[0]) +
                   float(line_split[2]) / 60 +
                   float(line_split[4]) / 3600)
            if line_split[6] == "W":
                lon = -lon
            break

    return {"lat": lat, "lon": lon}


def parse_lte_console(console):
    lteinfo = dict()

    lteinfo.update(get_gstatus(console))
    lteinfo.update(get_lteinfo(console))
    lteinfo.update(get_gpsloc(console))

    return lteinfo


def measure_lte():
    try:
        modem_console_path = config.get("modem_console_path")

        with serial.Serial(
            modem_console_path,
            baudrate=115200,
            timeout=5
        ) as ser:
            return parse_lte_console(ser)
    except Exception as e:
        raise LteError("Lteinfo acquire failed {}".format(e)) from e

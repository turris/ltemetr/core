class LtemetrError(Exception):
    pass


class ConfigError(LtemetrError):
    pass


class RunError(LtemetrError):
    """ A data parse or connection lost error. It could be recovered,
    measurement usually continues normally.
    """
    pass


class LteError(RunError):
    pass


class IperfError(RunError):
    pass


class NoInternet(Exception):
    """ Raised by measurement tool in case of connection lost that is expected
    to be recovered."""
    pass

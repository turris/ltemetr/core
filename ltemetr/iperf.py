import pexpect

from .config import config
from .exceptions import IperfError, NoInternet
from .logging import get_logger

NO_CONNECTION_THRESHOLD = 10


logger = get_logger()


def iperf_cmd(url, port):
    return ("iperf3 -c {} -p {} -t 0 --forceflush -R -f k").format(url, port)


def iperf_cmd_auth(url, port, pubkey_path):
    return ("iperf3 -c {} -p {} -t 0 --forceflush -R --username 'client'"
            " --rsa-public-key-path '{}' -f k").format(url, port, pubkey_path)


class Iperf:
    def __init__(self, port):
        self._no_connection_counter = 0
        authenticated = bool(config.get("iperf_server_authenticated"))
        url = config.get("iperf_server_address")

        if authenticated:
            pubkey_path = config.get("iperf_server_key_path")
            password = config.get("iperf_server_pass")
            cmd = iperf_cmd_auth(url, port, pubkey_path)
            logger.debug("Spawning iperf with cmd: '{}' and passwd: '{}'"
                         "".format(cmd, password))
        else:
            cmd = iperf_cmd(url, port)
            logger.debug("Spawning iperf with cmd: '{}'".format(cmd))

        self.child = pexpect.spawn(cmd)

        try:
            if authenticated:
                self.child.expect("Password:")
                self.child.sendline(password)

            # Expected non error output example:
            #   Connecting to host localhost, port 5200
            #   Reverse mode, remote host localhost is sending
            # Expected error output examples:
            # - iperf3: parameter error - must either be a client (-c) or server (-s)
            # - iperf3: error - unable to connect to server: Name or service not known

            i = self.child.expect(["iperf3: parameter error.*(?=Usage)",
                                   "iperf3: error.*",
                                   "Connecting.*sending"])
            if i < 2:
                error_str = self.child.after.decode("utf-8").strip()
                raise IperfError(error_str)

            # Expected non error output example:
            #   [  5] local ::1 port 37314 connected to ::1 port 5200
            # Expected error output:
            #   iperf3: error - test authorization failed

            i = self.child.expect(["iperf3.*error.*$", "\A.*connected"])  # noqa: W605
            if i == 0:
                error_str = (self.child.before + self.child.after).decode("utf-8").strip()
                raise IperfError(error_str)
            self.child.expect("Transfer.*Bitrate\s*\n")  # noqa: W605

        except (pexpect.exceptions.EOF, pexpect.exceptions.TIMEOUT) as e:
            self._clean_up()
            raise IperfError("Connecting to the iperf server failed") from e

    def _clean_up(self):
        self.child.close()

    def _reset_counter(self):
        self._no_connection_counter = 0

    def _increment_counter(self):
        self._no_connection_counter += 1
        if self._no_connection_counter >= NO_CONNECTION_THRESHOLD:
            self._clean_up()
            raise IperfError("Connection lost")

    def _get_rate(self, line):
        """ Return bitrate in Kbits/sec parsed from line which originally looks like:
        '[  5]   9.00-10.00  sec  6.42 GBytes  55188264 Kbits/sec'
        and is passed to this function as (using child.after)
        '[  5]   9.00-10.00  sec  6.42 GBytes  55188264 '
        """
        line = line.decode("utf-8")
        rate = line.split()[-1]
        if rate == "0.00":
            raise NoInternet()
        return int(float(rate))

    def get_data(self):
        try:
            while True:
                # Expected measument line example:
                #   [  5]   3.00-4.00   sec  10.1 GBytes  86928353 Kbits/sec
                # Expected end of measurement line example:
                #   - - - - - - - - - - - - - - - - - - - - - - - - -
                i = self.child.expect(["\A- - - - -.*\n", "Kbits/sec\s*\n"])  # noqa: W605
                if i == 0:
                    logger.debug("Ending measurement")
                    return
                try:
                    rate = self._get_rate(self.child.before)
                    self._reset_counter()
                    yield rate
                except NoInternet:
                    self._increment_counter()
                    yield 0

        except (pexpect.exceptions.EOF, pexpect.exceptions.TIMEOUT) as e:
            self._clean_up()
            raise IperfError("Unexpected end of Iperf measurement") from e

    def stop(self):
        self._clean_up()

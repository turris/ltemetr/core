import sys
import logging
import logging.handlers
from sentry_sdk.integrations.logging import LoggingIntegration

formatter = logging.Formatter(
    "ltemetr: %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    "%Y-%m-%d %H:%M:%S"
)
time_formatter = logging.Formatter(
    "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    "%Y-%m-%d %H:%M:%S"
)

syslog_handler = logging.handlers.SysLogHandler(address="/dev/log")
syslog_handler.setFormatter(formatter)
syslog_handler.setLevel(logging.INFO)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(time_formatter)
stream_handler.setLevel(logging.DEBUG)

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
root_logger.addHandler(syslog_handler)
root_logger.addHandler(stream_handler)

sentry_logging = LoggingIntegration(
    level=logging.INFO,
    event_level=logging.ERROR
)


def log_uncaught(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    root_logger.exception("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    sys.__excepthook__(exc_type, exc_value, exc_traceback)


sys.excepthook = log_uncaught

get_logger = logging.getLogger

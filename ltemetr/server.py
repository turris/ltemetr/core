import requests
import json

from .config import config
from .exceptions import RunError
from .logging import get_logger


logger = get_logger()


def get_port():
    try:
        url = "https://{}/create".format(config.get("iperf_server_address"))
        passwd = config.get("iperf_server_pass")
        response = requests.post(url, auth=("client", passwd))
        response.raise_for_status()

        if response and response.json().get("success", False):
            port = response.json().get("port")

            if port and type(port) is int:
                logger.debug("Got a new server port - %s", port)
                return port

            else:
                raise RunError("Getting port failed.")

        else:
            error = response.json().get("error", False)
            if error:
                raise RunError("Server port request failed ({})".format(error))
            else:
                raise RunError("Server port request failed with code {}"
                               "".format(response.status_code))

    except (ValueError, requests.HTTPError, requests.exceptions.ConnectionError) as e:
        raise RunError("Server port request failed ({})".format(e)) from e

    except json.decoder.JSONDecodeError as e:
        raise RunError("Failed to get JSON from server response ({})".format(e)) from e

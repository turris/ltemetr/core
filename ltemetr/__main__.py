from sys import stderr

import sentry_sdk

from .http_server import run_server
from .misc import activate_gps
from .logging import sentry_logging


PORT = 9090
SENTRY_DSN = "https://337f9bcdd5864304b421d9aef985d0d3:931471f833a34e1f966f80214dc0e69b@sentry.labs.nic.cz/44"  # noqa E501


def main():
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[sentry_logging]
    )
    activate_gps()

    try:
        run_server(port=PORT)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        stderr.write(str(e) + "\n")
        sentry_sdk.capture_exception(e)


if __name__ == "__main__":
    main()

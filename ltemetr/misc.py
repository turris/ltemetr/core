import datetime
import math
import os

from .config import config
from .exceptions import LtemetrError
from .logging import get_logger

logger = get_logger()


def now():
    """ Use this function to ensure we use the same timezone everywhere
    """
    return datetime.datetime.now()


def activate_gps():
    try:
        fd = os.open(config.get("gps_ctrl_console_path"), os.O_WRONLY)
        os.write(fd, "$GPS_START\n".encode("ascii"))
        os.close(fd)
    except (LtemetrError, OSError) as e:
        logger.error("Failed to command gps to start (%s)", e)


def distance(coord1, coord2):
    """ Distance in meters
    From https://janakiev.com/blog/gps-points-distance-python/
    Using Haversine Formula
    """
    R = 6372800  # Earth radius in meters
    lat1, lon1 = coord1
    lat2, lon2 = coord2

    phi1, phi2 = math.radians(lat1), math.radians(lat2)
    dphi = math.radians(lat2 - lat1)
    dlambda = math.radians(lon2 - lon1)

    a = math.sin(dphi / 2) ** 2 + \
        math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2

    return 2 * R * math.atan2(math.sqrt(a), math.sqrt(1 - a))

from .default_settings import default_conf
from .exceptions import ConfigError, LtemetrError
from .logging import get_logger
from .uci import UCIConfig

UCI_CONF_NAME = "ltemetr.settings"

logger = get_logger()


def get_bool(val):
    if isinstance(val, int):
        return bool(val)

    elif isinstance(val, str):
        if val in ["False", "false", "0"]:
            return False
        elif val in ["True", "true", "1"]:
            return True

    raise ConfigError("Boolean value not recognized ({})".format(val))


def convert(val, type):
    if type == bool:
        return get_bool(val)

    return type(val)


class Config:
    """ All the configuration variables must have a default value stated in
    the default config. Otherwise they are ignored.
    """
    def __init__(self):
        self.uci = UCIConfig(UCI_CONF_NAME)
        self._cache = {}
        self.reload()

    def reload(self):
        for key, def_val in default_conf.items():
            try:
                val = self.uci.get(key)

                if val is None:
                    raise ConfigError("Not defined")

                else:
                    self._cache[key] = convert(val, type(def_val))

            except (TypeError, ConfigError) as e:
                logger.error("%s not loaded from uci (%s), falling back to: "
                             "'%s'", key, e, def_val)
                self._cache[key] = def_val

    def get(self, key):
        if key in self._cache:
            return self._cache[key]
        else:
            raise LtemetrError("Requesting unknown config key ({})".format(key))

    def set(self, key, val):
        """ Allow modifying already existing items only.
        """
        if key in self._cache and self._cache[key] != val:
            self._cache[key] = val
            try:
                self.uci.set(key, val)
            except ConfigError as e:
                logger.error("%s=%s not saved via uci (%s)", key, val, e)

    def dump(self):
        return self._cache


config = Config()

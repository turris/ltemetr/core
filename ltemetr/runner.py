import threading
from datetime import datetime

from .config import config
from .exceptions import LtemetrError, RunError
from .logging import get_logger
from .lteinfo import get_operator
from .measurement import Measurement
from .measurement import MeasurementTypes as MT
from .netmetr import run_netmetr

logger = get_logger()


def upload_measurement(measurement):
    # TODO do something!
    pass


class StoppableThread(threading.Thread):
    def __init__(self, args):
        super(StoppableThread, self).__init__(args=args)
        self.runner = args[0]
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def join(self, *args, **kwargs):
        self.stop()
        super(StoppableThread, self).join(*args, **kwargs)

    def run(self):
        self.runner.errors = []

        try:
            if config.get("measurement_type") == MT.TRAIN:
                self._run_train_measurement()
            elif config.get("measurement_type") == MT.CAR:
                self._run_car_measurement()
            else:
                raise LtemetrError("Unknown measurement type ({})"
                                   "".format(self.runner.measurement_type))

        except LtemetrError as e:
            logger.exception("Measurement terminated due to: %s", e)
            self.runner.errors = ["Measurement terminated due to: {}".format(e)]
            self.runner.thread = None

    def _run_train_measurement(self):
        while not self._stop_event.is_set():
            measurement = Measurement(
                type=MT.TRAIN,
                stop_event=self._stop_event.is_set,
                duration=config.get("train_measurement_duration")
            )
            self.runner.measurement = measurement
            measurement.measure()
            if not self._stop_event.is_set():
                upload_measurement(measurement)
                try:
                    run_netmetr()
                except RunError as e:
                    logger.error(str(e))

    def _run_car_measurement(self):
        measurement = Measurement(
            type=MT.CAR,
            stop_event=self._stop_event.is_set,
            duration=0
        )
        self.runner.measurement = measurement
        measurement.measure()


class Runner():
    thread = None
    measurement = Measurement()
    errors = []

    try:
        operator = get_operator(config.get("modem_console_path"))
    except LtemetrError as e:
        operator = "Unknown"
        logger.error("Operator get failed (%s)", e)

    def start(self):
        if self.thread is None:
            self.thread = StoppableThread(args=(self,))
            self.thread.start()
            running = self.thread.is_alive()
        else:
            running = self.thread.is_alive()
        return running

    def stop(self):
        if self.thread is not None:
            self.thread.join()
            running = self.thread.is_alive()
            self.thread = None
        else:
            running = False
        return running

    def status(self):
        if self.thread is not None:
            running = self.thread.is_alive()
        else:
            running = False
        return running

    def export(self):
        return "\n".join(self.measurement.log)

    def get_export_filename(self):
        current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        return "ltemetr_{}_{}.csv".format(self.operator, current_time)

    def get_latest(self):
        if not self.measurement.last_time:
            return {}
        latest = self.measurement.last_lte
        latest.update({
            "paused": self.measurement.paused,
            "time": "{:%d.%m.%Y %H:%M:%S}".format(self.measurement.last_time),
            "IP Thrpt": self.measurement.last_kbps,
        })
        return latest

    def get_settings(self):
        return config.dump()

    def save_settings(self, settings):
        logger.debug("Received new settings: {}".format(settings))

        for key, value in settings.items():
            config.set(key, value)

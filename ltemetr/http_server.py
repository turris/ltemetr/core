from http.server import BaseHTTPRequestHandler, HTTPServer
import re
import json
import sentry_sdk

from .config import config
from .logging import get_logger
from .measurement import MeasurementTypes as MT
from .runner import Runner

logger = get_logger()

runner = Runner()


class Handler(BaseHTTPRequestHandler):
    def _is_route(self, pattern):
        return re.search(pattern, self.path) is not None

    def _set_headers(self, code=200):
        self.send_response(code)
        self.send_header("Content-type", "application/json")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()

    def _send_json(self, data, code=200):
        self._set_headers(code=code)
        self.wfile.write(str.encode(json.dumps(data)))

    def do_HEAD(self):
        self._set_headers()
        return

    def do_POST(self):
        if self._is_route("/start"):
            try:
                status = runner.start()
                self._send_json({"success": True, "running": status})
            except Exception as e:
                sentry_sdk.capture_exception(e)
                self._send_json({"success": False, "error": str(e)})
            return
        if self._is_route("/stop"):
            try:
                status = runner.stop()
                self._send_json({"success": True, "running": status})
            except Exception as e:
                sentry_sdk.capture_exception(e)
                self._send_json({"success": False, "error": str(e)})
            return
        if self._is_route("/settings"):
            try:
                length = int(self.headers["Content-Length"])
                data = self.rfile.read(length)
                runner.save_settings(json.loads(data.decode("utf-8")))
                self._send_json({
                    "success": True,
                    "settings": runner.get_settings()
                })
            except Exception as e:
                sentry_sdk.capture_exception(e)
                self._send_json({
                    "success": False,
                    "error": str(e),
                    "settings": runner.get_settings()
                })
            return
        else:
            self._set_headers(code=400)
            return

    def do_GET(self):
        if self._is_route("/export"):
            self.send_response(200)
            self.send_header("Content-type", "application/csv")
            filename = runner.get_export_filename()
            self.send_header("Content-disposition", "attachment; filename={}".format(filename))
            self.end_headers()
            csv = runner.export()
            self.wfile.write(str.encode(csv))
            return
        if self._is_route("/latest"):
            latest = runner.get_latest()
            self._send_json(latest)
            return
        if self._is_route("/status"):
            self._send_json({
                "success": True,
                "running": runner.status(),
                "errors": runner.errors,
                "operator": runner.operator,
            })
            return
        if self._is_route("/settings"):
            settings = runner.get_settings()
            self._send_json(settings)
            return
        else:
            self._set_headers(code=400)
            return


def run_server(port):
    if config.get("measurement_type") == MT.TRAIN:
        runner.start()
    server_address = ("", port)
    httpd = HTTPServer(server_address, Handler)
    logger.info("Starting HTTP server on port {}…".format(port))
    httpd.serve_forever()

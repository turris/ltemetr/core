import os
import subprocess

from .exceptions import ConfigError


class UCIConfig():
    def __init__(self, conf_name, bin_path="/sbin/uci"):
        self._conf_name = conf_name
        self._bin_path = bin_path

    def _check_binary(self):
        if not os.path.isfile(self._bin_path):
            raise ConfigError("uci binary at {} not found".format(self._bin_path))

    def get(self, var):
        self._check_binary()

        process = subprocess.Popen(
            ["uci", "-q", "get", "{}.{}".format(self._conf_name, var)],
            stdout=subprocess.PIPE
        )
        if process.wait() == 0:
            return process.stdout.read()[:-1].decode()

    def set(self, var, value):
        self._check_binary()

        retcode = subprocess.call([
            "uci", "set",
            "{}.{}={}".format(self._conf_name, var, value)
        ])
        if retcode != 0:
            raise ConfigError("uci set: nonzero return code ({})".format(retcode))

        retcode = subprocess.call(["uci", "commit"])
        if retcode != 0:
            raise ConfigError("uci commit: nonzero return code ({})".format(retcode))

    def delete(self, var):
        self._check_binary()

        retcode = subprocess.call([
            "uci", "-q", "delete",
            "{}.{}".format(self._conf_name, var)
        ])
        if retcode != 0:
            raise ConfigError("uci delete: nonzero return code ({})".format(retcode))

        retcode = subprocess.call(["uci", "commit"])
        if retcode != 0:
            raise ConfigError("uci commit: nonzero return code ({})".format(retcode))
